# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse
from .models import target_amount_tbl
import json


# Create your views here.

def target_amount(request, current_goal_amount, inflation_rate, period_of_investment):
    result = int(current_goal_amount) * (1 + float(inflation_rate) / 12) ** (int(period_of_investment))
    obj = target_amount_tbl(current_goal_amount=current_goal_amount, inflation_rate=inflation_rate,
                            period_of_investment=period_of_investment, t_amount=result)
    obj.save()
    return HttpResponse(result)


def target_amount_child(request, child, current_goal_amount, inflation_rate, period_of_investment):
    t_amount = int(child) * int(current_goal_amount) * (1 + int(inflation_rate) / 12) ** (int(period_of_investment))
    return HttpResponse(t_amount)


def target_amount_retirement(request, current_expense, lifestyle_factor, inflation_rate, period_of_investment):
    t_amount = int(current_expense) * int(lifestyle_factor) * (1 + int(inflation_rate) / 12) ** (
        int(period_of_investment))
    return HttpResponse(t_amount)


def target_amount_investment_monthly(request, target_amount, rate_of_return, investment_period):
    investment = int(target_amount) * (
            (int(rate_of_return) / 12) / (1 + (int(rate_of_return) / 12) ** (int(investment_period) * 12) - 1))
    return HttpResponse(investment)


def target_amount_investment_annual(request, target_amount, rate_of_return, investment_period):
    investment = int(target_amount) * (
            (int(rate_of_return)) / ((1 + int(rate_of_return)) ** (int(investment_period)) - 1))
    return HttpResponse(investment)


def display_table(request):
    var = target_amount_tbl.objects.all()
    e = ''
    for value in var:
        e += "id: " + str(value.id) + " | current_goal_amount: " + str(
            value.current_goal_amount) + " | inflation_rate: " + str(
            value.inflation_rate) + " | period_of_investment: " + str(
            value.period_of_investment) + " | t_amount: " + str(
            value.t_amount) + "<br>"
    return HttpResponse(json.dumps(e), content_type='application/json')
